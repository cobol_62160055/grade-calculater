       IDENTIFICATION DIVISION. 
       PROGRAM-ID. GRADE.
       AUTHOR. Chenpob.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT GRADE-FILE ASSIGN TO "mygrade.txt"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-GRADE-FILE ASSIGN TO "avg.txt"
             ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  GRADE-FILE.
       01  SUBJECT-DETAIL.
           88  END-OF-STU-FILE  VALUE HIGH-VALUE .
           05  CODE-SUBJECT   PIC X(6).
           05  NAME-SUBJECT   PIC X(50).
           05  CREDIT-SUBJECT PIC 9(1).
           05  GRADE-SUBJECT  PIC X(2).
           05  GRADE-NUM      PIC 9(1)V9(1).
       FD  AVG-GRADE-FILE.
       01  AVG-GRADE-DETAIL.
           05 AVG-NAME    PIC X(20).
           05 AVG-GRADE   PIC 9.999.

       WORKING-STORAGE SECTION.

       01  AVG-GRADE-ALL PIC 9(1)V9(3).
       01  SUM-POINT-ALL PIC 9(3)V9(3).  
       01  CREDIT-NUM-ALL  PIC 9(3).

       01  AVG-SCI-GRADE PIC 9(1)V9(3).
       01  SUM-POINT-SCI PIC 9(3)V9(3).  
       01  CREDIT-NUM-SCI   PIC 9(3).

       01  AVG-CS-GRADE PIC 9(1)V9(3).
       01  SUM-POINT-CS PIC 9(3)V9(3).  
       01  CREDIT-NUM-CS  PIC 9(3).

       PROCEDURE DIVISION .
       000-BEGIN.
           OPEN INPUT GRADE-FILE
           OPEN OUTPUT AVG-GRADE-FILE 
           
           PERFORM UNTIL END-OF-STU-FILE 
             READ GRADE-FILE
               AT END SET END-OF-STU-FILE TO TRUE 
             END-READ 
             IF NOT END-OF-STU-FILE THEN 
                PERFORM 001-GRADE-CAL THRU 001-EXIT 
                PERFORM 002-AVG-CAL THRU 002-EXIT 
             END-IF  
           END-PERFORM 

           PERFORM 006-WRITE-AVG THRU 006-EXIT 

           CLOSE AVG-GRADE-FILE 
           CLOSE GRADE-FILE 
           GOBACK 
           .
       001-GRADE-CAL.
           EVALUATE TRUE
            WHEN GRADE-SUBJECT = "A" MOVE 4 
              TO GRADE-NUM 
            WHEN GRADE-SUBJECT = "B+" MOVE 3.5 
              TO GRADE-NUM 
            WHEN GRADE-SUBJECT = "B" MOVE 3
              TO GRADE-NUM 
            WHEN GRADE-SUBJECT = "C+" MOVE 2.5
              TO GRADE-NUM 
            WHEN GRADE-SUBJECT = "C" MOVE 2 
              TO GRADE-NUM 
            WHEN GRADE-SUBJECT = "D+" MOVE 1.5
              TO GRADE-NUM 
            WHEN GRADE-SUBJECT = "D" MOVE 1 
              TO GRADE-NUM 
            WHEN GRADE-SUBJECT = "F" MOVE 0 
              TO GRADE-NUM 
           END-EVALUATE
           .
       001-EXIT.
           EXIT 
           .

       002-AVG-CAL.
           PERFORM 003-AVG-SUM-ALL THRU 003-EXIT 
           PERFORM 004-AVG-SUM-SCI THRU 004-EXIT 
           PERFORM 005-AVG-SUM-CS  THRU 005-EXIT 
           .
       002-EXIT.
           EXIT
           .
       003-AVG-SUM-ALL.
           COMPUTE SUM-POINT-ALL = SUM-POINT-ALL 
                                 + (GRADE-NUM * CREDIT-SUBJECT) 
           COMPUTE CREDIT-NUM-ALL  = CREDIT-NUM-ALL  + CREDIT-SUBJECT  
           .
       003-AVG-CAL-ALL.
           COMPUTE AVG-GRADE-ALL = SUM-POINT-ALL  / CREDIT-NUM-ALL  
           .
       003-EXIT.
           EXIT.

       004-AVG-SUM-SCI.
           IF CODE-SUBJECT IN SUBJECT-DETAIL (1:1) IS EQUAL TO "3" THEN
            COMPUTE SUM-POINT-SCI = SUM-POINT-SCI 
                                 + (GRADE-NUM * CREDIT-SUBJECT) 
            COMPUTE CREDIT-NUM-SCI  = CREDIT-NUM-SCI + CREDIT-SUBJECT 
           END-IF  
           .
       004-AVG-CAL-ALL.
           COMPUTE AVG-SCI-GRADE = SUM-POINT-SCI  / CREDIT-NUM-SCI   
           .
       004-EXIT.
           EXIT.
       
       005-AVG-SUM-CS.
           IF CODE-SUBJECT IN SUBJECT-DETAIL (1:2) IS EQUAL TO "31" THEN
            COMPUTE SUM-POINT-CS = SUM-POINT-CS
                                 + (GRADE-NUM * CREDIT-SUBJECT) 
            COMPUTE CREDIT-NUM-CS  = CREDIT-NUM-CS + CREDIT-SUBJECT 
           END-IF  
           .
       005-AVG-CAL-CS.
           COMPUTE AVG-CS-GRADE = SUM-POINT-CS  / CREDIT-NUM-CS   
           .
       005-EXIT.
           EXIT.
          
       006-WRITE-AVG.
           MOVE "AVG-GRADE" TO AVG-NAME 
           MOVE AVG-GRADE-ALL TO AVG-GRADE 
           WRITE AVG-GRADE-DETAIL 

           MOVE "AVG-SCI-GRADE" TO AVG-NAME 
           MOVE AVG-SCI-GRADE  TO AVG-GRADE 
           WRITE AVG-GRADE-DETAIL 

           MOVE "AVG-CS-GRADE" TO AVG-NAME 
           MOVE AVG-CS-GRADE  TO AVG-GRADE 
           WRITE AVG-GRADE-DETAIL 
           .
       006-EXIT.
           EXIT .
       
       
      